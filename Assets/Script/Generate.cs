﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Generate : MonoBehaviour {

	public Transform Brick;

	public int Count = 10;
	public GameObject checker;

	public GameObject Cube;

	public Transform Health, Coin;

	private int instant;
	// Use this for initialization
	void Start () {
		instant = Count;
		checker.transform.position = new Vector3(0,0,instant+1);

		
	}

	void Awake()
	{
		for (int i = 0; i < Count; i++)
		{
			Instantiate(Brick,new Vector3(0,0,i*2.0F),Quaternion.identity);
		}
		Instantiate(checker,new Vector3(0,0,instant),Quaternion.identity);

	}
	
	// Update is called once per frame
	void Update () {
		
			instant +=1;
			Instantiate(Brick,new Vector3(0,0,instant),Quaternion.identity);
		

		while(instant%30==0){
			int pos = UnityEngine.Random.Range(-1,1);
			Instantiate(Cube,new Vector3(pos,0.5f,instant),Quaternion.identity);
			break;
		}

		while(instant%70==0){
			StartCoroutine("Spawner");
			break;
		}


	}

IEnumerator Spawner(){
	yield return new WaitForSeconds(1f);
	int spawn = UnityEngine.Random.Range(0,6);
	int pos = UnityEngine.Random.Range(-1,1);
			switch(spawn){
				case 0:
				case 2:
					Instantiate(Health,new Vector3(pos,1,instant),Health.rotation);
				break;
				case 1:
				case 3:
					Instantiate(Coin,new Vector3(pos,1,instant),Quaternion.identity);
				break;
			}
	}

}
