﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class Menu : MonoBehaviour {

	// Use this for initialization
	public Text howtoText;
	void Start () {
		howtoText.enabled = false;
		StopAllCoroutines();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void StartGame(){
		SceneManager.LoadScene("first");
	}

	public void HowTo(){
		StartCoroutine("HowToGame");
	}

	public void Quit(){
		Application.Quit();
	}

	IEnumerator HowToGame(){
		howtoText.enabled = true;
		howtoText.text = "Игра представляет собой Endless runner. Клавиши A и D - для движения влево-вправо. Клавиша E - прибавляет здоровье. Цель - как можно дольше пробежать, собирая монеты и используя аптечки.";
		yield return new WaitForSeconds(20f);
		howtoText.enabled = false;
	}
}
