﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Quest : MonoBehaviour {

	private Health health;
	private int type;
	public Text questText;

	public float distance = 200, score = 300;
	/* enum Type{Dist, Score} */
	// Use this for initialization
	void Start () {
		health = (Health)gameObject.GetComponentInParent(typeof(Health));
		questText.enabled = false;
		questText.text = "";
		type = UnityEngine.Random.Range(0,3);

	}
	
	// Update is called once per frame
	void Update () {
		switch(type){
			case 0:
			case 2:
				StartCoroutine("Dist");
			break;
			case 1:
			case 3:
				StartCoroutine("Score");
			break;
			default:
				StopAllCoroutines();
			break;
		}
	}

	IEnumerator Dist(){
		questText.enabled = true;
		questText.text = "Преодолейте " + distance + " метров";
        yield return new WaitForSeconds(1.0f);
		if (health.Get_Dist() >= distance){
			health.Set_IsWin(true);
		}
	}


	IEnumerator Score(){
		questText.enabled = true;
		questText.text = "Наберите "+ score +" очков";
        yield return new WaitForSeconds(1.0f);
		if (health.Get_Score() >= score){
			health.Set_IsWin(true);
		}
	}
}
