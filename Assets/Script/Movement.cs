﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Movement : MonoBehaviour {


	private float horizVel = 0;

	
	public bool isMove = true;

	void Start(){

	}

public bool Get_IsMove(){return isMove;}
public void Set_IsMove(bool value){isMove = value;}

	void Update(){

		if(Input.GetKeyDown(KeyCode.A)){
			horizVel = -4;
			StartCoroutine(stopSlide());
		}

		if(Input.GetKeyDown(KeyCode.D)){
			horizVel = 4;
			StartCoroutine(stopSlide());
		}

		if(isMove){
			GetComponent<Rigidbody>().velocity = new Vector3(horizVel, 0, 4);
		} else {
			GetComponent<Rigidbody>().velocity = Vector3.zero;
		}

/* 		if (Input.GetKeyDown(KeyCode.F))
			{
				isMove = true;
				StopAllCoroutines();
			} */
	}
/* 
	void OnTriggerEnter(Collider col)
	{
		if (col.gameObject.tag == "Trigger")
		{
			isMove = false;
			StartCoroutine(StartCountdown());
		}
	}

	void OnTriggerExit(Collider col)
	{
		if (col.gameObject.tag == "Trigger")
		{
			isMove = true;
		}
	} */

float currCountdownValue;
 public IEnumerator StartCountdown(float countdownValue = 10)
 {
	 countdownValue = 10;
     currCountdownValue = countdownValue;
     while (currCountdownValue > 0)
     {
        yield return new WaitForSeconds(1.0f);
        currCountdownValue--;
     };		
	 if(currCountdownValue==0){
		 isMove = true;
	 }

 }
 
 IEnumerator stopSlide(){
	yield return new WaitForSeconds(1f);
	horizVel = 0;
 }
}
