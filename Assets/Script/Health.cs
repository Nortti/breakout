﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class Health : MonoBehaviour {

	public Slider healthBarSlider;  //reference for slider
	private bool isGameOver = false;

	private bool isWin = false;

	private bool isPause = false;

	private Movement movement;
	public GUISkin skin; 
	public Text distText,healthText, scoreText;
	

	Vector3 lastPosition;


	private int health = 0;
	private int score = 0;
	private float dist;//flag to see if game is over


public float Get_Dist(){return dist;}

public float Get_Score(){return score;}

public void Set_IsWin(bool value){isWin = value;}

	void Start(){
		movement = (Movement)gameObject.GetComponentInParent(typeof(Movement));
		lastPosition = transform.position;

	}

	// Update is called once per frame
	void Update () {


		if(health > 0 && healthBarSlider.value < healthBarSlider.maxValue){
			if(Input.GetKeyDown(KeyCode.E)){
				health -=1;
				healthBarSlider.value +=1;
				healthText.text = health.ToString();

			}
		}


		//check if game is over i.e., health is greater than 0
		if(!isGameOver) {
			transform.Translate(Input.GetAxis("Horizontal")*Time.deltaTime*10f, 0, 0); //get input
		} else {
			movement.Set_IsMove(false);
		}


			if(healthBarSlider.value<=0){
				isGameOver = true;//set game over to true
			}
		if(movement.Get_IsMove()){
			dist += Vector3.Distance(transform.position, lastPosition);
  			lastPosition = transform.position;
			
			distText.text = Mathf.Round(dist).ToString()+" м.";
		}

		if (Input.GetKeyDown(KeyCode.Escape))
		{
			isPause = true;
		}
	}

		
		void OnTriggerEnter(Collider col)
		{
			if (col.gameObject.tag == "Health")
			{
				health +=1;
				healthText.text = health.ToString();

				Destroy(col.gameObject);
			}

			if (col.gameObject.tag == "Trigger")
			{
				healthBarSlider.value -=1f;
				Destroy(col.gameObject);
			}

			if (col.gameObject.tag == "Coin")
			{
				score +=100;
				scoreText.text = "Очки: " + score.ToString();
				
				Destroy(col.gameObject);
			}
		}
		public void Restart(){
			SceneManager.LoadScene("first");

		}

		public void Exit(){
			Application.Quit();
		}
			//print("up arrow key is held down");

	

		void OnGUI() {
			GUI.skin = skin;
			if(isGameOver){
				GUI.Label(new Rect(Screen.width/2,0,300,20), dist.ToString());
				GUI.Box(new Rect(Screen.width/2-100,Screen.height/2, 200, 150),"Игра окончена");
				if (GUI.Button(new Rect(Screen.width/2-75, Screen.height/2+50, 150, 40), "Повторить")){
            		Restart();
				}
				if (GUI.Button(new Rect(Screen.width/2-75, Screen.height/2+100, 150, 40), "Выйти")){
            		Exit();
				}
			}

			if(isWin){
				GUI.Box(new Rect(Screen.width/2-100,Screen.height/2, 200, 150),"Вы победили!");
				if (GUI.Button(new Rect(Screen.width/2-75, Screen.height/2+50, 150, 40), "Повторить")){
            		Restart();
				}
				if (GUI.Button(new Rect(Screen.width/2-75, Screen.height/2+100, 150, 40), "Выйти")){
            		Exit();
				}
				movement.Set_IsMove(false);
			}

			if(isPause){
				GUI.Box(new Rect(Screen.width/2-100,Screen.height/2, 200, 150),"Пауза");
				if (GUI.Button(new Rect(Screen.width/2-75, Screen.height/2+50, 150, 40), "Повторить")){
            		Restart();
				}
				if (GUI.Button(new Rect(Screen.width/2-75, Screen.height/2+100, 150, 40), "Выйти")){
            		Exit();
				}
				movement.Set_IsMove(false);
			}
        
    }



	 
}

